/*
 * midinator.c
 *
 * (c) 2012 Thomas White <taw@bitwiz.org.uk>
 *
 * This file is part of MIDInator.
 *
 * MIDInator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MIDInator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MIDInator.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include <unistd.h>

#include "fifo.h"


static void show_help(const char *s)
{
	printf("Syntax: %s [options]\n\n", s);
	printf(
"Do MIDI stuff\n"
"\n"
" -h, --help              Display this help message.\n"
);
}


struct wargs
{
	FIFO *f;
	int fd;
};


static unsigned char get_midi(FIFO *f)
{
	do {

		int err;
		char c;
		c = fifo_pop(f, &err);
		if ( err == 0 ) return c;

		usleep(1);

	} while ( 1 );
}


static void *process_midi(void *pv)
{
	struct wargs *w = pv;
	unsigned char c;
	int ch, cmd;
	FIFO *f;
	int fd;

	f = w->f;
	fd = w->fd;

	do {

		c = get_midi(f);

		if ( c == 0xfe ) {
			unsigned char sbuf[32];
			sbuf[0] = 0xfe;
			write(fd, sbuf, 1);
			continue;
		}

		ch = c & 0x0f;
		cmd = c & 0xf0;


		if ( cmd == 0x90 ) {

			unsigned char sbuf[32];
			int note, vel;

			note = get_midi(f);
			vel = get_midi(f);

			sbuf[0] = 0x91;
			sbuf[1] = note - 12;
			sbuf[2] = vel >> 1;

			write(fd, sbuf, 3);

		}

		/* Control change or mode message */
		if ( cmd == 0xb0 ) {

			int ctrl, data;

			ctrl = get_midi(f);
			data = get_midi(f);

			if ( ctrl == 0x40 ) {
				unsigned char sbuf[32];
				sbuf[0] = 0xb1;
				sbuf[1] = 0x40;
				sbuf[2] = data;
				write(fd, sbuf, 3);
			}

		}

		/* Program change */
		if ( cmd == 0xc0 ) {

			unsigned char sbuf[32];
			int prog;

			prog = get_midi(f);

			sbuf[0] = 0xc1;
			sbuf[1] = prog;

			write(fd, sbuf, 2);

		}

		/* Yamaha panel data */
		if ( c == 0xf0 ) {

			unsigned char v;
			unsigned char buf[32];
			int i = 0;

			printf("Panel data: ");
			buf[i++] = 0xf0;
			do {
				v = get_midi(f);
				buf[i++] = v;
				if ( v != 0xf7 ) {
					printf("%02hhx ", v);
				}
			} while ( v != 0xf7 );
			printf("\n");

			write(fd, buf, i);

		}

	} while ( 1 );

	return NULL;
}


static void midi_readable(int fd, FIFO *f)
{
	int rval, i;
	unsigned char buf[32];

	rval = read(fd, buf, 32);

	if ( (rval == -1) || (rval == 0) ) {
		fprintf(stderr, "Read error.\n");
		return;
	}

	for ( i=0; i<rval; i++ ) {
		fifo_push(f, buf[i]);
	}

}


int main(int argc, char *argv[])
{
	int c;
	int fd;
	int rval;
	FIFO *f;
	pthread_t thread;
	struct wargs wargs;

	/* Long options */
	const struct option longopts[] = {
		{"help",               0, NULL,               'h'},
		{0, 0, NULL, 0}
	};

	/* Short options */
	while ((c = getopt_long(argc, argv, "h",
	                        longopts, NULL)) != -1) {

		switch (c) {
		case 'h' :
			show_help(argv[0]);
			return 0;

		case 0 :
			break;

		default :
			return 1;
		}

	}

	fd = open("/dev/snd/midiC1D0", O_RDWR);
	if ( fd == -1 ) {
		fprintf(stderr, "Couldn't open MIDI device.\n");
		return 1;
	}

	int i;
	for ( i=0; i<8; i++ ) {
		unsigned char sbuf[32];
		sbuf[0] = 0xb0 || i;
		sbuf[1] = 0x78;
		sbuf[2] = 0x00;
		write(fd, sbuf, 3);
	}

	f = fifo_new();
	if ( f == NULL ) {
		fprintf(stderr, "Couldn't create FIFO.\n");
		exit(1);
	}

	wargs.f = f;
	wargs.fd = fd;
	if ( pthread_create(&thread, NULL, process_midi, (void *)&wargs) ) {
		fprintf(stderr, "Couldn't start processing thread\n");
		exit(1);
	}

	do {

		fd_set fds;
		struct timeval tv;
		int sval;

		FD_ZERO(&fds);
		FD_SET(fd, &fds);

		tv.tv_sec = 1;
		tv.tv_usec = 0;

		sval = select(fd+1, &fds, NULL, NULL, &tv);

		rval = 0;
		if ( sval == -1 ) {
			int err = errno;
			fprintf(stderr, "select() failed: %s\n", strerror(err));
			rval = 1;
		} else if ( sval != 0 ) {

			midi_readable(fd, f);

		} /* else timeout */

	} while ( !rval );

	close(fd);

	pthread_join(thread, NULL);

	return 0;
}
