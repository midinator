/*
 * fifo.h
 *
 * (c) 2012 Thomas White <taw@bitwiz.org.uk>
 *
 * This file is part of MIDInator.
 *
 * MIDInator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MIDInator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MIDInator.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef FIFO_H
#define FIFO_H

typedef struct _fifo FIFO;

extern FIFO *fifo_new();
extern void fifo_push(FIFO *f, unsigned char c);
extern unsigned char fifo_pop(FIFO *f, int *err);

#endif /* FIFO_H */
