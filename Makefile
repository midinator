PROGS  = midinator

CFLAGS = -O2 -Wall -g  ${INCLUDES}
CAIRO_CFLAGS=${CFLAGS} `pkg-config --cflags cairo` -pthread
GTK_CFLAGS=${CAIRO_CFLAGS} `pkg-config --cflags gtk+-2.0`

LIBS = -pthread
CAIRO_LIBS=${LIBS} `pkg-config --libs cairo`
GTK_LIBS=${CAIRO_LIBS} `pkg-config --libs gtk+-2.0`

all: ${PROGS}

midinator: midinator.o fifo.o
	gcc midinator.o fifo.o -o midinator ${LIBS}

midinator.o: midinator.c
	gcc -c midinator.c -o midinator.o ${CFLAGS}

fifo.o: fifo.c
	gcc -c fifo.c -o fifo.o ${CFLAGS}

clean:
	rm -f ${PROGS} *.o

.PHONEY: all
